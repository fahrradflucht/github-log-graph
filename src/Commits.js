import React, { Component } from 'react';
import Commit from './Commit';
import { getCommits } from './helpers/githubApi';
import './Commits.css';

class Commits extends Component {
  constructor(props) {
    super(props)
    this.state = { commits: [] };
  }
  
  componentDidMount() {
    const { owner, repo } = this.props;
    getCommits( owner, repo )
    .then(commits => {
      this.setState({ commits });
    });
  }

  render() {
    const { commits } = this.state;
    return (
      <div className="Commits">
        {commits.map(commit => (
          <Commit commit={commit} key={commit.sha} />
        ))}
      </div>
    );
  }
}

export default Commits;
