import React, { Component } from 'react';
import './Commit.css';

class Commit extends Component {
  render() {
    const { commit } = this.props.commit;
    return (
      <p className="Commit">
        {commit.message}
      </p>
    );
  }
}

export default Commit;