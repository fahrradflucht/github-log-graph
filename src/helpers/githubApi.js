function repoURL(owner, repo) {
  return `https://api.github.com/repos/${owner}/${repo}/commits?per_page=100`;
}

export function getCommits(owner, repo) {
  return fetch(repoURL(owner, repo), {
    headers: new Headers({
      'Accept': 'application/vnd.github.v3+json',
      'Authorization': 'token 219d8bb866e273c809a49a5dc8bae39d173dc446',
      'User-Agent': 'GithubLogGraph'
    })
  })
  .then(response => {
    console.log(
      'X-RateLimit-Remaining:',
      response.headers.get('X-RateLimit-Remaining')
    )
    return response.json()
  })
  .catch(err => { console.log(err) })
};
