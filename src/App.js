import React, { Component } from 'react';
import './App.css';
import Commits from './Commits';

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="App-header">
          <h2>Github Log Graph</h2>
        </div>
        <Commits owner={"Fahrradflucht"} repo={"vcp-hamburg-theme"} />
      </div>
    );
  }
}

export default App;
